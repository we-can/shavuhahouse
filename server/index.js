require('dotenv').config()
const express = require('express');
const sequelize = require('./db')
const models = require('./models/module')
const cors = require('cors')

const PORT = process.env.PORT || 3000;

const app = express();
app.use(cors())
app.use(express.json())

const start = async () => {
    try {
        await sequelize.authenticate()
        await sequelize.sync()
        app.listen(PORT, () => console.log(`server started on post ${PORT}`))
    } catch (e) {
        console.log(e)
    }
}

start()
