const sequelize = require('../db')
const {DataTypes} = require('sequelize')

const Clients = sequelize.define('clients',{
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    phone: {type: DataTypes.STRING(11), unique: true},
    address: {type: DataTypes.STRING}
})

const Orders = sequelize.define('orders', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    status: {type: DataTypes.STRING},
    stream: {type: DataTypes.STRING}
})

const OrdersProduct = sequelize.define('orders_product', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true}
})

const Users = sequelize.define('users', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    email: {type: DataTypes.STRING, unique: true},
    password: {type: DataTypes.STRING},
    role: {type: DataTypes.STRING, defaultValue: "USER"}
})

const Personnel = sequelize.define('personnel', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, allowNull: false},
    surname: {type: DataTypes.STRING, allowNull: false},
    position: {type: DataTypes.STRING},
    rateInHour: {type: DataTypes.FLOAT},
    status: {type: DataTypes.BOOLEAN}
})

const Branches = sequelize.define('branches', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, allowNull: false},
    address: {type: DataTypes.STRING, allowNull: false},
    revenue: {type: DataTypes.FLOAT},
    email: {type: DataTypes.STRING, unique: true},
    phone: {type: DataTypes.STRING(11), unique: true},
})

const Storehouse = sequelize.define('storehouse', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    productName: {type: DataTypes.STRING, unique: true, allowNull: false},
    amount: {type: DataTypes.INTEGER, defaultValue: 0}
})

Clients.hasOne(Orders)
Orders.belongsTo(Clients)

Orders.hasMany(OrdersProduct)
OrdersProduct.belongsTo(Orders)

Personnel.hasOne(Users)
Users.belongsTo(Personnel)

Branches.hasMany(Personnel)
Personnel.belongsTo(Branches)

Branches.hasMany(Storehouse)
Storehouse.belongsTo(Branches)

Storehouse.hasOne(OrdersProduct)
OrdersProduct.belongsTo(Storehouse)

module.exports = {
    Clients,
    Orders,
    OrdersProduct,
    Storehouse,
    Users,
    Personnel,
    Branches
}